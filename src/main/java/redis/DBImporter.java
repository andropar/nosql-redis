package redis;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonStreamParser;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;

public class DBImporter {

	private static final boolean CLUSTERED = false;
	private static JedisCluster jedisCluster;
	private static Jedis jedis; 
	
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		// Read json file into parser
		String fileName = "/business.json";
		InputStream is = DBImporter.class.getResourceAsStream(fileName);
		Reader reader = new InputStreamReader(is, "UTF-8");
		JsonStreamParser jsonStreamParser = new JsonStreamParser(reader);
		
		// Connect to redis db
		if (CLUSTERED) {
			Set<HostAndPort> clusterNodes = new HashSet<HostAndPort>();
			clusterNodes.add(new HostAndPort("localhost", 30001));
			jedisCluster = new JedisCluster(clusterNodes);
		} else {
			jedis = new Jedis("localhost");
		}
		
		Integer counter = 0;
		// Iterate over all json objects in parser
		while(jsonStreamParser.hasNext()) {
			JsonElement jsonElement = jsonStreamParser.next();
			if(jsonElement.isJsonObject()) {
				JsonObject jsonObject = (JsonObject) jsonElement;
				String businessId = jsonObject.get("business_id").getAsString();
				
				jedis.set(counter.toString(), businessId);
				counter++;
				
				// Check type of all values in json object and persist to db accordingly
				for (String key : jsonObject.keySet()) {
					String uniqueKey = "business:" + businessId + ":" + key;
					if (jsonObject.get(key).isJsonObject()) {
						persistAsHashMap(jsonObject, key, uniqueKey);
					} else if (jsonObject.get(key).isJsonArray()) {
						persistAsSet(jsonObject, key, uniqueKey);
					} else {
						persistAsValue(jsonObject, key, uniqueKey);
					}
				}
				System.out.println("Persisted business with key: business:" + businessId + ":...");	
			}
		}
		System.out.println("Imported whole dataset into Redis!");
	}
	
	private static void persistAsHashMap(JsonObject jsonObject, String key, String uniqueKey) {
		HashMap<String, String> hashmap = new HashMap<String, String>();
		JsonObject innerJsonObject = (JsonObject) jsonObject.get(key);
		if(innerJsonObject.keySet().size() > 0) {
			for (String innerKey : innerJsonObject.keySet()) {
				if (innerJsonObject.get(innerKey).isJsonObject()) {
					String innerUniqueKey = uniqueKey + ":" + innerKey;
					persistAsHashMap((JsonObject) innerJsonObject, innerKey, innerUniqueKey);
				} else {
					hashmap.put(innerKey, innerJsonObject.get(innerKey).getAsString());
				}
			}
			if(!hashmap.isEmpty()) {
				jedis.hmset(uniqueKey, hashmap);
				System.out.println(uniqueKey + ": " + hashmap);
			}
		}	
	}
	
	private static void persistAsSet(JsonObject jsonObject, String key, String uniqueKey) {
		for (JsonElement item : (JsonArray) jsonObject.get(key)) {
			jedis.sadd(uniqueKey, item.getAsString());
		}
		System.out.println(uniqueKey + ": " + (JsonArray) jsonObject.get(key));
	}
	
	private static void persistAsValue(JsonObject jsonObject, String key, String uniqueKey) {
		if (!jsonObject.get(key).isJsonNull()) {
			String value = jsonObject.get(key).getAsString();
			System.out.println(uniqueKey + ": " + value);
			jedis.set(uniqueKey, value);
		}
	}
}
