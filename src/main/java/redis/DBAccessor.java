package redis;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;

public class DBAccessor {

	private static final boolean INIT = false;
	private static final boolean CLUSTERED = false;
	private static JedisCluster jedisCluster;
	private static Jedis jedis;
	private static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		// Connect to redis db
		if(CLUSTERED) {
			Set<HostAndPort> clusterNodes = new HashSet<HostAndPort>();
			clusterNodes.add(new HostAndPort("localhost", 30001));
			jedisCluster = new JedisCluster(clusterNodes);
		} else {
			jedis = new Jedis("localhost");
		}
		
		if(INIT) {
			prepareData();
		} else {
			boolean userExit = false;
			while(!userExit) {
				System.out.println("Please choose one of the following options:");
				System.out.println("(1) Search for company names using auto completion");
				System.out.println("(2) Exit");
				String userInput = input.next();
				
				if(isNumeric(userInput)) {	
					switch(Integer.parseInt(userInput)) {
						case 1: searchForCompanyName(); break;
						case 2: userExit = true; break;
					}
				} else {
					System.out.println("Please enter a number!");
				}
			}
		}	
	}
	
	private static void searchForCompanyName() {
		System.out.println("Please enter part of the companies name:");
		String companyNamePart = input.next();
		final ArrayList<String> companyNames = new ArrayList<String>();
		
		if (CLUSTERED) {
			ScanParams scanParams = new ScanParams().count(1000).match("{" + companyNamePart + "*}");
			String cursor = "0";
			do {
				ScanResult<String> scanResult = jedisCluster.scan(cursor, scanParams);
				scanResult.getResult().stream().forEach(companyName -> companyNames.add(companyName));
				cursor = scanResult.getStringCursor();
			} while(!cursor.equals("0"));
		} else {
			getCompanyNamesFromRedis(companyNamePart, companyNames);
		}
		
		if (companyNames.size() > 0) {
			companyNames.forEach(companyName -> System.out.println(companyName));
		} else {
			System.out.println("I couldn't find any companies.");
		}
		
		System.out.println("Would you like to get more information about a particular company? (Y/N)");
		String userInput = input.next();
		switch(userInput) {
			case "Y": getCompanyInformation(); break;
			case "N": break;
		}
		
	}
	
	
	
	private static void getCompanyInformation() {
		System.out.println("Please enter the name of the company you want to know more about:");
		String userInput = input.next();
		
		Set<String> businessIds = jedis.smembers(userInput);
		
		if (businessIds.isEmpty()) {
			System.out.println("I couldn't find any companies with this name.");
		} else {
			System.out.println("Here is what I could find about this company:");
			System.out.println(userInput + " has " + businessIds.size() + " shops around the world.");
			System.out.println(userInput + "s shops are also listed under the following categories:");
			jedis.sunion(getArrayFromSetOfBusinessIds(businessIds, "categories")).forEach(category -> System.out.println(category));
			System.out.println("You can find the shops here:");
			businessIds.forEach(businessId -> printBusinessLocation(businessId));
			double averageRating = getAverageRating(businessIds);
			int reviewerCount = getReviewerCount(businessIds);
			System.out.println("They have an average rating of " + averageRating + " Stars from " + reviewerCount + " reviewers.");
		}
		
	}
	
	private static double getAverageRating(Set<String> businessIds) {
		double averageRating = 0;
		for (String businessId : businessIds) {
			averageRating += Double.parseDouble(jedis.get("business:" + businessId + ":stars"));
		}
		return averageRating / businessIds.size();
	}
	
	private static int getReviewerCount(Set<String> businessIds) {
		int reviewerCount = 0;
		for (String businessId : businessIds) {
			reviewerCount += Integer.parseInt(jedis.get("business:" + businessId + ":review_count"));
		}
		return reviewerCount;
	}

	private static void printBusinessLocation(String businessId) {
		String firstPart = "business:" + businessId + ":";
		String location = jedis.get(firstPart + "address") + ", " + jedis.get(firstPart + "postal_code") + " " 
							+ jedis.get(firstPart + "city") + ", " + jedis.get(firstPart + "state");
		System.out.println(location);
	}

	private static void getCompanyNamesFromRedis(String companyNamePart, ArrayList<String> companyNames) {
		jedis.keys(companyNamePart + "*").forEach(companyName -> companyNames.add(companyName)); 
	}
	
	private static String[] getArrayFromSetOfBusinessIds(Set<String> businessIds, String suffix) {
		int counter = 0;
		String[] unionStringArray = new String[businessIds.size()];
		for(String businessId : businessIds) {
			unionStringArray[counter] = "business:" + businessId + ":" + suffix;
			counter++;
		}
		return unionStringArray;
	}
	
	
	
	private static void prepareData() {
		boolean finished = false;
		Integer counter = 0;
		
		if(!CLUSTERED) {
			while(!finished) {
				if(jedis.get(counter.toString()) != null) {
					String businessId = jedis.get(counter.toString());
					String companyName = jedis.get("business:" + businessId + ":name");
					if(isNumeric(companyName)) {
						// Can't use numbers as key for sets (?)
					} else {
						jedis.sadd(companyName, businessId);
						System.out.println("Added " + companyName + " with id " + businessId + " to redis.");
					}
					counter++;
				} else {
					finished = true;
				}
			}	
		} else {
			while(!finished) {
				if(jedisCluster.get(counter.toString()) != null) {
					String businessId = jedisCluster.get(counter.toString());
					String companyName = jedisCluster.get("business:" + businessId + ":name");
					if(isNumeric(companyName)) {
						// Can't use numbers as key for sets (?)
					} else {
						jedisCluster.sadd("{" + companyName + "}", businessId);
						System.out.println("Added " + companyName + " with id " + businessId + " to redis.");
					}
					counter++;
				} else {
					finished = true;
				}
			}
		}
	}
	
	public static boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    double d = Double.parseDouble(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
}
