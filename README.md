# NoSQL - Redis

To use this repository as stated in the exercise sheet, set up a redis db on your local machine (either standalone or as a cluster) and start it using the command line. 

If not already done, you at first have to import the business dataset into your redis db. For this, run the DBImporter class once, either with CLUSTERED set to true or false. If you use CLUSTERED = true, you also have to rename "jedis" to "jedisCluster".

Then run the DBAccessor class once with INIT on true and CLUSTERED depending on your setup. After that you can query your redis db with the console menu after running the class with INIT on false. 
